package local.library.web.service;

import local.library.web.SessionFactory;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * EntityManagerService
 */
@Service
public class EntityManagerService {
    public static EntityManager getManager() {
        EntityManagerFactory entityManagerFactory = SessionFactory.getInstance();
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        return entityManager;
    }
}
