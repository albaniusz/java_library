package local.library.web;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * SessionFactory
 *
 * @since 0.1
 */
public class SessionFactory {
    /**
     * EntityManagerFactory
     */
    private static EntityManagerFactory entityManagerFactory;

    /**
     * Init entity manager factory
     *
     * @return EntityManagerFactory
     */
    public static EntityManagerFactory getInstance() {
        if (entityManagerFactory == null) {
            entityManagerFactory = Persistence.createEntityManagerFactory("local.sandbox.hibernate.jpa");
        }

        return entityManagerFactory;
    }

    /**
     * Close entity manager
     */
    public static void close() {
        entityManagerFactory.close();
    }
}
